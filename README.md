# Bridging the Gap Between Basic Research and Applied Bioinformatics

## Date, time, and locatrion

October 4, 2019, from 2:30 pm to 3:30 pm. The Seminar Series of the Department of Bioinformatic and Genomics (BiG), College of Computing anf Informatics (CCI), University of North Carolina at Charlotte (UNC Charlotte).

## Author

Denis Jacob Machodo, ORCID [0000-0001-9858-4515](https://orcid.org/0000-0001-9858-4515), email dmachado@uncc.edu.

## Abstract

Researchers interested in basic research on non-model organisms would not dispute that their fields often struggle to catch up with the cutting edge techniques routinely employed in applied bioinformatics. Not surprisingly, we observe an increase in demand for bioinformaticians in places that perhaps they never considered working, such as in a natural history museum. However, this is not a one-way road, as applied bioinformatics also benefits from basic research. In this talk, I will showcase novel methodologies in bioinformatics that arose due to collaborations among researchers with distinct backgrounds. Specifically, we will focus on how the assembly of mitochondrial genomes for taxonomic and phylogenetic purposes led to the creation of new indexes and algorithms that we are applying to improve viral genome annotation with potential impacts on the surveillance for emerging viral threats.

## Availability

- DOI: [10.5281/zenodo.3468530](https://doi.org/10.5281/zenodo.3468530)
- License: [CC BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/)
- GitLab: [gitlab.com/MachadoDJ/talk-big-2019](https://gitlab.com/MachadoDJ/talk-big-2019)

## Compilation notes

Copile with `pdflatex`.